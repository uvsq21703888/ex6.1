public final class Fraction
{
    private final int numerateur;
    private final int denominateur;
    /**
     * Constantes ZERO et UN 
     */
    public final static Fraction ZERO = new Fraction(0, 1);
    public final static Fraction UN = new Fraction(1, 1);
   
    /**
     * Constructor for objects of class Fraction
     */
    public Fraction()
    {
        this(0, 0);
    }
    /**
     * @param numerateur est un int
     */
    public Fraction(int numerateur)
    {
        this(numerateur, 1);
    }
    /**
     * @param numerateur est un int
     * @param denominateur est un int
     */
